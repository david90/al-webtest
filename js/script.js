// Constants
var POLL_TIME_OUT = 15 * 60; // 15 mins
var poller;
var graphEle = null;

// Native functions
var getRecordTemplate = function (name, mentionCount) {
    var rowString = "<div class=\"leaderboard-row\"><div class=\"name\">{{name}}</div><div class=\"mentions\">{{mentionCount}} <span class=\"mentions-suffix\">{{mentionSuffix}}</span></div></div>";
    rowString = rowString.replace("{{name}}", name);
    rowString = rowString.replace("{{mentionCount}}", mentionCount);
    rowString = (mentionCount == 1) ? rowString.replace("{{mentionSuffix}}", "Mention") : rowString.replace("{{mentionSuffix}}", "Mentions");
    var rowEle = $(rowString);
    return rowEle;
  }

var putDataIntoGraph = function (graphData) {
  graphData.forEach(updateRecord);
}

var updateGraph = function(callbackData) {
  reloadGraph(callbackData);
}

var updateRecord = function (recordData) {
  var rowEle = getRecordTemplate(recordData.name, recordData.count);
  rowEle.appendTo(graphEle);
}

var emptyGraphItems = function () {
  graphEle.empty();
}

var reloadGraph = function (graphData) {
  var reloadBtn = $(".reload");
  reloadBtn.fadeOut(100);
  graphEle.slideUp(400);

  graphEle.promise().done(function() {
    emptyGraphItems();
    putDataIntoGraph(graphData);
    graphEle.slideDown(400);
    reloadBtn.fadeIn(800);
  });
}

var startPoll = function () {
      poller = new massrel.Poller({
      frequency: POLL_TIME_OUT,
      limit: 5
    }, updateGraph);
    poller.start();
}

var stopPoll = function() {
    if(poller !== null && poller !== undefined) {
      poller.stop();
    }
}

var reloadPoll = function () {
  stopPoll();
  startPoll();
}

$().ready(function(){
    graphEle = $(".leaderboard");
    startPoll();
    $(".reload").on("click",reloadPoll);
  }
);